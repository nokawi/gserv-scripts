#!/bin/bash

CLIENT_FILE=$(find / -name cliente_fb_backup.cfg |head -1)

if [ -z $CLIENT_FILE ]; then

    echo "arquivo de configuracao do cliente nao foi encontrado..."

    exit 1

else

    source $CLIENT_FILE

fi

if [ -z "$SSH_PORT" ]; then

    SSH_PORT_PRM="22"

elif [ "$SSH_PORT" != "22" ]; then

    SSH_PORT_PRM="-p $SSH_PORT"

fi

if [ -z "$SSH_FTP_PORT" ]; then

    SSH_FTP_PORT_PRM="21"

elif [ "$SSH_FTP_PORT" != "21" ]; then

    SSH_FTP_PORT_PRM="-P $SSH_FTP_PORT"

fi

function cleanup {
    rm -rf /tmp/$cada.errormessage.sql
    rm -rf /tmp/$cada.tmp
    rm -rf /tmp/$cada.newequip.sql
    rm -rf /tmp/status-$cada.sql
    rm -rf /tmp/envia_backup.tmp
}

function backup {

    databkp="$(date +%Y%m%d)-$(date +%H%M)"

    for cada in $HOSTLIST; do

        echo "Executando backup em $cada"

        unset dev_type
        unset device_type

        dev_type=$(echo $cada |cut -f2 -d";" |tr A-Z a-z)

        cada=$(echo $cada |cut -f1 -d";")

        if [ -e $KNOCK_PORTS ]; then

            for port in $(echo $KNOCK_PORTS | sed 's/,/\n/g'); do

                ssh -o StrictHostKeyChecking=no -p $KNOCK_PORTS -o BatchMode=yes -o ConnectTimeout=1 $SSH_USER@$cada echo ok 2>&1

            done

        fi

        if [ "$(nmap $cada -p $SSH_PORT |grep "$SSH_PORT/tcp" |awk {'print $2'})" = "open" ]; then

            function cli_mikrotik {

                device_type="mikrotik"

                device_name=$(sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada "system identity print" |grep name |awk -d":" {'print $2'} | tr -d '\r')

                sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada 'system resource print' > /tmp/$cada.tmp

                sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada 'system routerboard print' >> /tmp/$cada.tmp

                device_curr_firmware=$(cat /tmp/$cada.tmp | grep "current-firmware" |awk -F":" {'print $2'} |sed 's/ //g')

                device_up_firmware=$(cat /tmp/$cada.tmp | grep "upgrade-firmware" |awk -F":" {'print $2'} |sed 's/ //g')

                device_os_version=$(cat /tmp/$cada.tmp | grep "version:" |awk -F":" {'print $2'} |sed 's/ //g')

                device_arch=$(cat /tmp/$cada.tmp | grep "architecture-name" |awk -F":" {'print $2'} |sed 's/ //g')

                device_model=$(cat /tmp/$cada.tmp | grep "board-name" |awk -F":" {'print $2'} |sed 's/ //g')

            }

            function cli_juniper {

                device_type="juniper"

                sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada 'show system information' > /tmp/$cada.tmp

                sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada 'show version' >> /tmp/$cada.tmp

                device_curr_firmware=$(cat /tmp/$cada.tmp | grep -i "JUNOS Kernel Software Suite" |awk -F"[" {'print $2'} |sed 's/ //g' |sed 's/]//g')

                device_os_version=$(cat /tmp/$cada.tmp | grep -i "JUNOS Base OS Software Suite" |awk -F"[" {'print $2'} |sed 's/ //g' |sed 's/]//g')

                device_model=$(cat /tmp/$cada.tmp | grep -i "Model" |awk -F":" {'print $2'} |sed 's/ //g')

                device_name=$(cat /tmp/$cada.tmp | grep -i "Hostname" |awk -F":" {'print $2'} |sed 's/ //g')
            }

            function cli_huawei {

                device_type="huawei"

                sshpass -p "" ssh -o StrictHostKeyChecking=no admin@$cada 'display system-information' > /tmp/$cada.tmp

                device_name=$(cat /tmp/$cada.tmp | grep -i "System Name" |awk -F":" {'print $2'} |sed 's/ //g')

                device_curr_firmware=$(cat /tmp/$cada.tmp | grep -i "Software Version" |awk -F":" {'print $2'} |sed 's/ //g')

                device_model=$(cat /tmp/$cada.tmp | grep -i "Board Type" |awk -F":" {'print $2'} |sed 's/ //g')
            }

            function cli_cisco {

                device_type="cisco"

            }

            function cli_ubiquiti {

                device_type="uniquiti"

            }

            if [ "$dev_type" = "mikrotik" ]; then

                cli_mikrotik

            elif [ "$dev_type" = "juniper" ]; then

                cli_juniper

            elif [ "$dev_type" = "huawei" ]; then

                cli_huawei

            elif [ "$dev_type" = "cisco" ]; then

                cli_cisco

            elif [ "$dev_type" = "ubiquiti" ]; then

                cli_ubiquiti

            else

                if [ "$(sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada 'system resource print' |grep platform |awk {'print $2'} |sed 's/ //g' |tr A-Z a-z | tr -d '\r')" = "mikrotik" ]; then

                    cli_mikrotik

                elif [ $(sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada 'show system information' |grep -i junos |awk {'print $2'} |sed 's/ //g' |tr A-Z a-z |tr -d '\r' |wc -l) -ge "1" ]; then

                    cli_juniper

                elif [ $(sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada |grep -i "System Name" |awk {'print $2'} |sed 's/ //g' |tr A-Z a-z |tr -d '\r') = "huawei" ]; then

                    cli_huawei

                elif [ $(sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada |grep -i hostname |awk {'print $2'} |sed 's/ //g' |tr A-Z a-z |tr -d '\r') = "cisco" ]; then

                    cli_cisco

                elif [ $(sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada |grep -i hostname |awk {'print $2'} |sed 's/ //g' |tr A-Z a-z |tr -d '\r') = "ubiquiti" ]; then

                    cli_ubiquiti

                fi

            fi

            function cria_backup {

                if [ "$device_type" = "mikrotik" ]; then

                    sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada "system backup save name=$device_name-$databkp ; export file=$device_name-$databkp"

                elif [ "$device_type" = "juniper" ]; then

                    sshpass -p " " ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "huawei" ]; then

                    sshpass -p " " ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "cisco" ]; then

                    sshpass -p " " ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "ubiquiti" ]; then

                    sshpass -p " " ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                fi

            }

            function remove_backup {

                if [ "$device_type" = "mikrotik" ]; then

                    sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada "/file remove $device_name-$databkp.rsc ; /file remove $device_name-$databkp.backup"

                elif [ "$device_type" = "juniper" ]; then

                    sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "huawei" ]; then

                    sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "cisco" ]; then

                    sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "ubiquiti" ]; then

                    sshpass -p "$SSH_PASS" ssh $SSH_PORT_PRM -o StrictHostKeyChecking=no $SSH_USER@$cada

                fi

                rm -rf /tmp/$cada.tmp

            }

            function baixa_backup {

                if [ "$device_type" = "mikrotik" ]; then

                    ncftpget -u $SSH_USER $SSH_FTP_PORT_PRM -p "$SSH_PASS" -A $cada $folder_backup_full "$device_name-$databkp.rsc" "$device_name-$databkp.backup"

                elif [ "$device_type" = "juniper" ]; then

                    sshpass -p "$SSH_PASS" ssh -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "huawei" ]; then

                    sshpass -p "$SSH_PASS" ssh -o StrictHostKeyChecking=no $SSH_PASS@$cada

                elif [ "$device_type" = "cisco" ]; then

                    sshpass -p "$SSH_PASS" ssh -o StrictHostKeyChecking=no $SSH_USER@$cada

                elif [ "$device_type" = "ubiquiti" ]; then

                    sshpass -p "$SSH_PASS" ssh -o StrictHostKeyChecking=no $SSH_USER@$cada

                fi

            }

            cria_backup
            baixa_backup
            remove_backup

        fi

    done

}

function envia_backup {

    if [ $(ncftpls -u $FTP_USER $SSH_FTP_PORT_PRM -p "$FTP_PASS" -t 3 ftp://$FTP_SERVER 2> /dev/null |wc -l) = "0" ]; then

        bkp_message="Servidor de FTP indisponível"

        cada=$(hostname)

        bkp_error

    else

        echo "cd backup-script" > /tmp/envia_backup.tmp
        echo "mkdir $ano-$mes" >> /tmp/envia_backup.tmp
        echo "cd $ano-$mes" >> /tmp/envia_backup.tmp
        echo "mkdir $dia" >> /tmp/envia_backup.tmp

        ncftp -u $FTP_USER_FB -p $FTP_PASS_FB $FTP_SERVER_FB < /tmp/envia_backup.tmp

        ncftpput -R -u $FTP_USER -p "$FTP_PASS" $FTP_SERVER /backup-script/$ano-$mes/$dia /var/ftp-$CLIENTE/backups/$ano-$mes/$dia/*

    fi
}

#SENHA_ZABBIX=$(cat /etc/zabbix/web/zabbix.conf.php |grep "PASSWORD" |awk {'print $3'} |sed "s/'//g" |cut -f1 -d';')
#HOSTLIST=$(mysql -u zabbix -p$SENHA_ZABBIX zabbix -e "select ip from interface;" |grep -v ip)
# mysql -u zabbix -pCri3WeprED zabbix -e " select interface.ip, hstgrp.name from interface inner join hosts_groups on interface.hostid = hosts_groups.hostid inner join hstgrp on hosts_groups.groupid = hstgrp.groupid;" |grep -v name |sed 's/\t/;/g' |tr 'A-Z' 'a-z' |grep -v 127.0.0.1 |sort -g

#HOSTLIST=$(mysql -u $ZBX_DB_USER -p$ZBX_DB_PASS -h $ZBX_DB_SERVER $ZBX_DB -e "select interface.ip, hstgrp.name from interface inner join hosts_groups on interface.hostid = hosts_groups.hostid inner join hstgrp on hosts_groups.groupid = hstgrp.groupid;" |grep -v name |sed 's/\t/;/g' |tr 'A-Z' 'a-z' |grep -v 127.0.0.1 |sort -g)

HOSTLIST=$(mysql -u $ZBX_DB_USER -p$ZBX_DB_PASS -h $ZBX_DB_SERVER $ZBX_DB -e "select ip from interface" |tr 'A-Z' 'a-z' |grep -v ip |grep -v 127.0.0.1 |sort -g)

dia=$(date +%d)
mes=$(date +%m)
ano=$(date +%Y)
hora=$(date +%H:%M)

if [ ! -d $folder_backup/$ano-$mes/$dia ]; then

    mkdir -p $folder_backup/$ano-$mes/$dia

fi

folder_backup_full="$folder_backup/$ano-$mes/$dia"

if [ ! -z "$folder_backup" ] && [ ! -z $dias_bkp ]; then

    find $folder_backup -mtime +$dias_bkp -exec rm -f {} \;
    find $folder_backup/OLT/*.cfg -mtime +$dias_bkp -exec rm -f {} \;
    find $folder_backup/ANM/*.cfg -mtime +$dias_bkp -exec rm -f {} \;

fi

backup

cleanup