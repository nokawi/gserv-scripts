#!/bin/bash

trap ctrl_c INT

# quando o usuário apertar ctrl c o script limpa o
# teste anterior, pula duas linhas e mostra a mensagem em vermelho
function ctrl_c() {
	echo "Operação cancelada pelo usuário."
	menu
}

function continuar {

    echo ""
    echo ""

    read -n 1 -s -r -p "pressione qualquer tecla para continuar..."

    clear

    menu

}

function cleanup {
    rm -rf /tmp/*.lock
}

function bashrc {
    yum install nano -y

    echo "
    alias ..='cd ..'
    alias rm='rm -i'
    alias cp='cp -i'
    alias mv='mv -i'

    alias grep='grep --color=auto'
    alias egrep='egrep --color=auto'
    alias fgrep='fgrep --color=auto'

    PS1='\[\033[01;31m\]\u\[\033[02;37m\]@\[\033[02;33m\]\h:\[\033[02;36m\]\w\$\[\033[00m\] '

    # Source global definitions
    if [ -f /etc/bashrc ]; then
            . /etc/bashrc
    fi

    " > /root/.bashrc

    cp /root/fb/conf.nanorc /usr/share/nano/

    find /usr/share/nano/ -iname "*.nanorc" -exec echo include {} \; >> /root/.nanorc

    continuar
}

function neofetch {

    yum install -y dnf && dnf -y install dnf-plugins-core && dnf -y copr enable konimex/neofetch && dnf -y install neofetch

    if [ ! -e "/etc/fbconsultoria.motd" ]; then

        echo " ______ ____ "                    >> /etc/fbconsultoria.motd
        echo "|  ____|  _ \   CONSULTORIA E"    >> /etc/fbconsultoria.motd
        echo "| |__  | |_) |  ASSESSORIA EM TI" >> /etc/fbconsultoria.motd
        echo "|  __| |  _ <"                    >> /etc/fbconsultoria.motd
        echo "| |    | |_) | (47) 3267-1823"    >> /etc/fbconsultoria.motd
        echo "|_|    |____/  (47) 3369-6731"    >> /etc/fbconsultoria.motd
        echo "               (47) 4102-1347"    >> /etc/fbconsultoria.motd
        echo ""                                 >> /etc/fbconsultoria.motd
        echo "suporte@fbconsultoria.net"        >> /etc/fbconsultoria.motd
        echo "www.fbconsultoria.net"            >> /etc/fbconsultoria.motd
        echo "Porto Belo - Santa Catarina"      >> /etc/fbconsultoria.motd

    fi

    if [ ! -f /var/spool/cron/root ]; then

        touch /var/spool/cron/root

        echo "*/1  *  *  *  *  /usr/bin/neofetch --shell_version off --shell_path off --source /etc/fbconsultoria.motd > /etc/motd" > /var/spool/cron/root && chmod 600 /var/spool/cron/root

    elif [ $(cat /var/spool/cron/root |grep "/var/spool/cron/root" |wc -l) = "0" ]; then

        echo "*/1  *  *  *  *  /usr/bin/neofetch --shell_version off --shell_path off --source /etc/fbconsultoria.motd > /etc/motd" > /var/spool/cron/root && chmod 600 /var/spool/cron/root

    fi

    systemctl restart crond

    continuar

}

function best_practices {

    if [ $(systemctl status firewalld |grep Active: | awk -d":" {'print $2'}) != "inactive" ]; then

        systemctl disable firewalld ; systemctl stop firewalld

    fi

    if [[ "$(cat /etc/selinux/config |grep '^SELINUX=' |cut -d"=" -f2)" =~ [Ee]nforcing ]]; then

        sed 's/[eE]nforcing/disabled/g' -i /etc/selinux/config

    fi

    if [ $(cat /etc/ssh/sshd_config |grep 4721 |wc -l) = "0" ]; then

        sed 's/#Port 22/Port 4721/g' -i /etc/ssh/sshd_config

    fi

    pacotes="
    htop
    net-tools
    net-snmp
    net-snmp-devel
    net-snmp-utils
    wget
    iputils
    traceroute
    vim
    nano
    mlocate
    epel-release
    bind-utils
    tcpdump
    iotop
    nload
    multitail
    telnet
    nmap
    "

    for package in $pacotes; do

        if [ $(rpm -qa --qf "%{NAME}\n" |egrep -w "$package" |head -1 |wc -l) = "0" ]; then

            pk_install="$pk_install@$package"

        fi

    done

    if [ ! -z $pk_install ]; then

        pk_install=$(echo $pk_install |sed 's/@/ /g')

        yum update -y && yum install $pk_install -y

    fi

    continuar

}

function install_zabbix {

    clear

    function nao_usada {

        echo ""
        echo ""
        echo " 1 - prod     | ambientes de fora da empresa que utilizarao o ip 186.251.23.230 na porta 9901"
        echo ""
        echo " 2 - dev      | ambientes internos que utilizarao o ip 192.168.2.248 na porta 8080"
        echo ""
        echo " 3 - local    | ambiente local de testes em localhost na porta 8080"
        echo ""
        echo ""
        read -p " Escolha uma das opcoes de ambiente para instalacao: " ENV

        case ENV in
            1)
                fb_server="186.251.23.230:9901"
            ;;

            2)
                fb_server="192.168.2.248:8080"
            ;;

            3)
                fb_server="localhost:8080"
            ;;
        esac

    }

    echo ""
    echo ""
    read -p "Digite o nome do Sevidor Zabbix: " NOME_ZABBIX
    echo ""
    echo ""

    counter="10"

    while [ "$counter" != "-1" ]; do

            echo -en "\r Iniciando instalacao do ambiente Zabbix em $counter segundos..."

            sleep 1

            let "counter=$counter-1"

    done

    echo ""
    echo ""

    echo "Iniciando configuracao do MariaDB..."

    yum install mariadb-server mariadb httpd php -y

    sed 's/\[mysqld]/\[mysqld]\nmax_connections = 2000/g' -i /etc/my.cnf.d/server.cnf

    systemctl start mariadb.service

    db_root_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    db_zabbix_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    echo "
    UPDATE mysql.user SET Password=PASSWORD('${db_root_password}') WHERE User='root';
    DELETE FROM mysql.user WHERE User='';
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
    DROP DATABASE IF EXISTS test;
    DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
    FLUSH PRIVILEGES;
    CREATE DATABASE zabbix CHARACTER SET utf8 COLLATE utf8_bin; 
    GRANT ALL PRIVILEGES ON zabbix.* TO 'zabbix'@'%' IDENTIFIED BY '${db_zabbix_password}';
    SET GLOBAL max_connections = 500;
    " > /tmp/mariadb.config.tmp && mysql -u root < /tmp/mariadb.config.tmp

    echo "Iniciando instalacao do Zabbix"

    # VERSAO 3.0
    # rpm -ivh https://repo.zabbix.com/zabbix/3.0/rhel/7/x86_64/zabbix-release-3.0-1.el7.noarch.rpm

    # Versao 4.0.2
    rpm -Uvh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-2.el7.noarch.rpm

    yum install zabbix-server-mysql zabbix-web-mysql zabbix-agent zabbix-get -y

    sed 's;# php_value date.timezone Europe/Riga;php_value date.timezone America/Sao_Paulo;g' -i /etc/httpd/conf.d/zabbix.conf

    sed "s;# DBPassword=;DBPassword=$db_zabbix_password;g" -i /etc/zabbix/zabbix_server.conf
    sed 's/# DBHost=localhost/DBHost=localhost/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# StartPollers=5/StartPollers=16/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# StartPollersUnreachable=1/StartPollersUnreachable=32/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# StartPingers=1/StartPingers=32/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# HousekeepingFrequency=1/HousekeepingFrequency=3/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# MaxHousekeeperDelete=5000/MaxHousekeeperDelete=0/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# CacheSize=8M/CacheSize=16M/g' -i /etc/zabbix/zabbix_server.conf

    zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -u zabbix --pass=$db_zabbix_password zabbix

    echo ""
    echo ""

    cp /root/fb/zabbix.conf.php /etc/zabbix/web/zabbix.conf.php
    sed "s;db_zabbix_password;$db_zabbix_password;g" -i /etc/zabbix/web/zabbix.conf.php
    sed "s;NOME_ZABBIX;$NOME_ZABBIX;g" -i /etc/zabbix/web/zabbix.conf.php

    echo ""
    read -p " Voce ja possui as informacoes do telegram para configuracao do mesmo? y/n" telegraminfo
    echo ""

    telegraminfo=$(echo $telegraminfo |tr A-Z a-z)

    while [ "$telegraminfo" != y ] && [ "$telegraminfo" != "n" ]; do

        echo "resposta incorreta, digite uma opcao correta: y ou n"
        echo ""
        read -p " Voce ja possui as informacoes do telegram para configuracao do mesmo? y/n" telegraminfo
        echo ""

    done

    if [ "$telegraminfo" = "y" ]; then

        echo ""
        read -p " Insira o ID do grupo do telegram: " GROUP_ID
        echo ""

        echo ""
        read -p " Insira o TOKEN do bot: " TOKEN_BOT
        echo ""

        cp /root/fb/zabbix-telegram.sh /usr/lib/zabbix/alertscripts/zabbix-telegram.sh

        chmod /usr/lib/zabbix/alertscripts/zabbix-telegram.sh

        IP_SERVER=$(ifconfig |grep -w inet |grep -v "127.0.0.1" |awk {'print $2'})

        mysql -u zabbix --pass=$db_zabbix_password zabbix -e "INSERT INTO media_type VALUES (4,1,'Telegram','','','','zabbix-telegram.sh','','','',0,25,0,0,0,0,'{ALERT.SENDTO}\n{ALERT.SUBJECT}\n{ALERT.MESSAGE}\n',1,3,'10s');"
        
        mysql -u zabbix --pass=$db_zabbix_password zabbix -e "INSERT INTO users VALUES (3,'telegram','telegram','','3e49316528a99784f57ea14741ecc046','',0,'0','en_GB','30s',3,'default',0,'',0,50);"

        mysql -u zabbix --pass=$db_zabbix_password zabbix -e "INSERT INTO media VALUES ('',3,4,'-"$GROUP_ID"',0,60,'1-7,00:00-24:00');"

        zbx_telegram_pass="UvRAUG7yUq4t"

        sed "s/IP_SERVER/$IP_SERVER/g" -i /usr/lib/zabbix/alertscripts/zabbix-telegram.sh

        sed "s/TOKEN_BOT/$TOKEN_BOT/g" -i /usr/lib/zabbix/alertscripts/zabbix-telegram.sh

    fi

    chown apache:apache /etc/zabbix/web/zabbix.conf.php

    systemctl enable httpd zabbix-server zabbix-agent mariadb

    echo ""
    echo "Instalacao concluida..."
    echo ""
    echo "Senha do MariaDB: $db_root_password"
    echo ""
    echo "Senha do Zabbix para DB: $db_zabbix_password"
    echo ""
    echo "Acesso WEB:"
    echo "  user = Admin"
    echo "  pass = zabbix (trocar no primeiro acesso para m7...)"
    echo ""
    echo ""
    echo " REINICIE O SERVIDOR PARA PODER ACESSAR O ZABBIX VIA WEB"

    continuar

}

function install_webmin {
    echo "[Webmin]" > /etc/yum.repos.d/webmin.repo
    echo "name=Webmin Distribution Neutral" >> /etc/yum.repos.d/webmin.repo 
    echo "baseurl=http://download.webmin.com/download/yum" >> /etc/yum.repos.d/webmin.repo
    echo "enabled=1" >> /etc/yum.repos.d/webmin.repo
    echo "gpgcheck=1" >> /etc/yum.repos.d/webmin.repo
    echo "gpgkey=http://www.webmin.com/jcameron-key.asc" >> /etc/yum.repos.d/webmin.repo
    
    yum -y install webmin

    sed 's/port=10000/port=8079/g' -i /etc/webmin/miniserv.conf
    sed 's/listen=10000/listen=8079/g' -i /etc/webmin/miniserv.conf
    sed 's/ipv6=1/ipv6=0/g' -i /etc/webmin/miniserv.conf

    continuar

}

function install_ftpbkp {

    clear

    read -p "Digite o nome do cliente para configuracao do servidor FTP (sem espacos ou caracteres nao imprimiveis): " NOME_CLIENTE

    yum install httpd php wget openssl-devel.x86_64 nmap mysql sshpass ncftp ftp mariadb-server mariadb -y

    systemctl start httpd ; systemctl enable httpd

    yum install proftpd -y

    echo "Port 4720" >> /etc/proftpd.conf

    systemctl enable proftpd && systemctl start proftpd

    if [ $(cat /etc/shells |grep "/sbin/nologin" |wc -l) = "0" ]; then

        echo "/sbin/nologin" >> /etc/shells

    fi

    mkdir -p /var/ftp-$NOME_CLIENTE/backup
    useradd $NOME_CLIENTE -M -d /var/ftp-$NOME_CLIENTE
    chown -R $NOME_CLIENTE:$NOME_CLIENTE /var/ftp-$NOME_CLIENTE
    chmod -R 0777 /var/ftp-$NOME_CLIENTE/

    ftp_pass=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    echo $NOME_CLIENTE:$ftp_pass | chpasswd

    if [ $(cat /var/spool/cron/root |grep '-mtime +90' |wc -l) = "0" ] && [ $(cat /var/spool/cron/root |grep '-mtime +90' |wc -l) -ge "1" ] ; then

        sed -i '/$NOME_CLIENTE/d' /var/spool/cron/root

        echo "0 6 * * * find /home/$NOME_CLIENTE/backup/*.rsc -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root
        echo "0 6 * * * find /home/$NOME_CLIENTE/backup/*.backup -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root
        echo "0 6 * * * find /home/$NOME_CLIENTE/ANM/*.zip -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root
        echo "0 6 * * * find /home/$NOME_CLIENTE/OLT/*.cfg -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root

        chmod 600 /var/spool/cron/root

        systemctl restart crond

    fi

    mkdir /var/www/html/ftp

    htpasswd -c -b /etc/httpd/.htpasswd fb "#fb#$NOME_CLIENTE"

    htpasswd -b /etc/httpd/.htpasswd $NOME_CLIENTE "$ftp_pass"

    chown apache:apache /etc/httpd/.htpasswd

    mkdir /var/www/html/ftp

    cp /root/fb/ftp_server.txt /var/www/html/ftp/index.php

    #wget http://186.251.23.230:9901/ftp_server.txt -O /var/www/html/ftp/index.php

    sed "s;/tmp/backup/;/var/ftp-$NOME_CLIENTE/backup/;g" -i /var/www/html/ftp/index.php

    echo '
    <Directory "/var/www/html/ftp">
        Options Indexes FollowSymLinks
        AuthType Basic
        AuthName "FTP Backup Server - FB Consultoria"
        AuthUserFile /etc/httpd/.htpasswd
        Require valid-user
    </Directory>
    ' > /etc/httpd/conf.d/ftp_server.conf

    echo "date.timezone = America/Sao_Paulo" >> /etc/php.ini

    systemctl restart httpd

    mkdir /root/auto_backup/
    cp /root/fb/backup-equipamentos-cliente.sh /root/auto_backup/
    cp /root/fb/cliente.cfg /root/auto_backup/cliente_fb_backup.cfg

    echo "00 00 * * * root /root/auto_backup/backup-equipamentos-cliente.sh" >> /etc/crontab 

    systemctl restart crond

    IPs=$(ifconfig |grep 'inet ' |grep -v "127.0.0.1" |awk {'print $2'})

    ftpbackup=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    systemctl enable httpd mariadb rsyslogd

    clear

    echo ""
    echo " Accessos:"
    for cada in $IPs; do
        echo "      http://$cada/ftp"
    done
    echo ""
    echo " FB user: fb"
    echo " FB pass: #fb#$NOME_CLIENTE"
    echo ""
    echo " Cliente user: $NOME_CLIENTE"
    echo " Cliente pass: $ftp_pass"
    echo ""
    echo ""
    echo " Senha de backup que devera ser configurada nos equipamentos para backup automatico: "
    echo " USER: ftpbackup" 
    echo " PASS: $ftpbackup"
    echo ""

    continuar

}

function install_rsyslog {

    curl http://rpms.adiscon.com/v8-stable/rsyslog.repo > /etc/yum.repos.d/rsyslog.repo && yum install rsyslog* --skip-broken -y

    yum install mariadb-server mariadb httpd wget php php-mysql php-gd -y

    sed 's/\[mysqld]/\[mysqld]\nmax_connections = 2000/g' -i /etc/my.cnf.d/server.cnf

    systemctl start mariadb.service

    db_root_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w16 | head -n1)

    db_syslog_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w16 | head -n1)

    echo "
    UPDATE mysql.user SET Password=PASSWORD('${db_root_password}') WHERE User='root';
    DELETE FROM mysql.user WHERE User='';
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
    DROP DATABASE IF EXISTS test;
    DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
    FLUSH PRIVILEGES;
    SET GLOBAL max_connections = 500;
    " > /tmp/mariadb.config.tmp && mysql -u root < /tmp/mariadb.config.tmp

    echo "
        CREATE DATABASE Syslog;
        USE Syslog;
        CREATE TABLE SystemEvents
        (
            ID int unsigned not null auto_increment primary key,
            CustomerID bigint,
            ReceivedAt datetime NULL,
            DeviceReportedTime datetime NULL,
            Facility smallint NULL,
            Priority smallint NULL,
            FromHost varchar(60) NULL,
            Message text,
            NTSeverity int NULL,
            Importance int NULL,
            EventSource varchar(60),
            EventUser varchar(60) NULL,
            EventCategory int NULL,
            EventID int NULL,
            EventBinaryData text NULL,
            MaxAvailable int NULL,
            CurrUsage int NULL,
            MinUsage int NULL,
            MaxUsage int NULL,
            InfoUnitID int NULL ,
            SysLogTag varchar(60),
            EventLogType varchar(60),
            GenericFileName VarChar(60),
            SystemID int NULL
        );

        CREATE TABLE SystemEventsProperties
        (
            ID int unsigned not null auto_increment primary key,
            SystemEventID int NULL ,
            ParamName varchar(255) NULL ,
            ParamValue text NULL
        );

    " > /tmp/mariadb.config.tmp && mysql -u root -p$db_root_password < /tmp/mariadb.config.tmp

    mysql -u root -p$db_root_password -e "GRANT ALL ON Syslog.* TO 'rsyslogdbadmin'@'localhost' IDENTIFIED BY '$db_syslog_password';"

    echo 'module(load="imuxsock")' > /etc/rsyslog.conf
    echo 'module(load="imklog")' >> /etc/rsyslog.conf
    echo 'module(load="imudp")' >> /etc/rsyslog.conf
    echo 'input(type="imudp" port="514")' >> /etc/rsyslog.conf
    echo 'module(load="imtcp")' >> /etc/rsyslog.conf
    echo 'input(type="imtcp" port="514")' >> /etc/rsyslog.conf
    echo 'module(load="ommysql")' >> /etc/rsyslog.conf
    echo '$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat' >> /etc/rsyslog.conf
    echo '$IncludeConfig /etc/rsyslog.d/*.conf' >> /etc/rsyslog.conf
    echo '*.info;mail.none;authpriv.none;cron.none /var/log/messages' >> /etc/rsyslog.conf
    echo 'authpriv.* /var/log/secure' >> /etc/rsyslog.conf
    echo 'mail.* /var/log/maillog' >> /etc/rsyslog.conf
    echo 'cron.* /var/log/cron' >> /etc/rsyslog.conf
    echo '*.emerg :omusrmsg:*' >> /etc/rsyslog.conf
    echo 'uucp,news.crit /var/log/spooler' >> /etc/rsyslog.conf
    echo 'local7.* /var/log/boot.log' >> /etc/rsyslog.conf
    echo "*.* :ommysql:127.0.0.1,Syslog,rsyslogdbadmin,$db_syslog_password" >> /etc/rsyslog.conf

    wget -q -O- $(curl https://loganalyzer.adiscon.com/downloads/ |grep '.tar.gz' |cut -f2 -d'"') | tar -xvzf -

    mkdir -p /var/www/html/log

    arquivo=$(ls |grep loganalyzer |grep -v .sql)

    cp -r $arquivo/src/* /var/www/html/log/
    cp -r $arquivo/contrib/* /var/www/html/log/

    echo '
    <Directory "/var/www/html/log">
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
    </Directory>
    ' > /etc/httpd/conf.d/log_server.conf

    systemctl restart rsyslog
    systemctl restart httpd

    cp /root/fb/config.php /var/www/html/log/ && chmod 666 /var/www/html/log/config.php

    sed "s/trocarsenha/$db_syslog_password/g" -i /var/www/html/log/config.php

    mysql -u root -p$db_root_password Syslog < /root/fb/loganalyzer.sql

    mysql -u root -p$db_root_password Syslog -e "update logcon_sources set DBPassword='"$db_syslog_password"' where DBUser='rsyslogdbadmin';"

    loganalyzer_admin_pass=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w16 | head -n1)

    mysql -u root -p$db_root_password Syslog -e "update logcon_users set password=MD5('"$loganalyzer_admin_pass"') where username='admin';"

    echo "mysql --user=rsyslogdbadmin --password="$db_syslog_password" --database=Syslog --execute='DELETE FROM Syslog.SystemEvents WHERE ReceivedAt < DATE_SUB(NOW(), INTERVAL 365 DAY);'" > /root/fb/limpalog.sh

    chmod +x /root/fb/limpalog.sh

    echo "00 00  1 * * root sh /root/fb/limpalog.sh" >> /etc/crontab && systemctl restart crond

    echo " root= $db_root_password"
    echo " rsyslogdbadmin = $db_syslog_password"
    echo " Admin (web) = $loganalyzer_admin_pass"

    continuar

}

function install_mtmon {

    clear

    read -p "Digite o nome do cliente/servidor: " NOME_CLIENTE

    while [ -z $NOME_CLIENTE ];do

        echo ""
        echo "O nome do cliente nao pode ficar em branco..."
        echo ""

        read -p "Digite o nome do cliente/servidor: " NOME_CLIENTE

    done

    yum -y install perl-CPAN perl-LWP-Protocol-https.noarch perl-Crypt-SSLeay.x86_64 perl-CGI.noarch perl-JSON.noarch perl-JSON-PP.noarch
    yum -y install rcs sharutils sysstat wget
    yum -y install gcc m4 awk flex bison expect

    unset PERL5LIB
    unset PERL_LOCAL_LIB_ROOT

    mkdir -p /root/.cpan/CPAN/ && touch /root/.cpan/CPAN/MyConfig.pm

    CPAN='$CPAN'

    echo "
    '$CPAN'::Config = {'
    'applypatch' => q[],
    'auto_commit' => q[0],
    'build_cache' => q[100],
    'build_dir' => q[/root/.cpan/build],
    'build_dir_reuse' => q[0],
    'build_requires_install_policy' => q[yes],
    'bzip2' => q[],
    'cache_metadata' => q[1],
    'check_sigs' => q[0],
    'colorize_output' => q[0],
    'commandnumber_in_prompt' => q[1],
    'connect_to_internet_ok' => q[1],
    'cpan_home' => q[/root/.cpan],
    'ftp_passive' => q[1],
    'ftp_proxy' => q[],
    'getcwd' => q[cwd],
    'gpg' => q[/usr/bin/gpg],
    'gzip' => q[/usr/bin/gzip],
    'halt_on_failure' => q[0],
    'histfile' => q[/root/.cpan/histfile],
    'histsize' => q[100],
    'http_proxy' => q[],
    'inactivity_timeout' => q[0],
    'index_expire' => q[1],
    'inhibit_startup_message' => q[0],
    'keep_source_where' => q[/root/.cpan/sources],
    'load_module_verbosity' => q[none],
    'make' => q[/usr/bin/make],
    'make_arg' => q[],
    'make_install_arg' => q[],
    'make_install_make_command' => q[/usr/bin/make],
    'makepl_arg' => q[],
    'mbuild_arg' => q[],
    'mbuild_install_arg' => q[],
    'mbuild_install_build_command' => q[./Build],
    'mbuildpl_arg' => q[],
    'no_proxy' => q[],
    'pager' => q[/usr/bin/less],
    'patch' => q[],
    'perl5lib_verbosity' => q[none],
    'prefer_external_tar' => q[1],
    'prefer_installer' => q[MB],
    'prefs_dir' => q[/root/.cpan/prefs],
    'prerequisites_policy' => q[follow],
    'scan_cache' => q[atstart],
    'shell' => q[/bin/bash],
    'show_unparsable_versions' => q[0],
    'show_upload_date' => q[0],
    'show_zero_versions' => q[0],
    'tar' => q[/usr/bin/tar],
    'tar_verbosity' => q[none],
    'term_is_latin' => q[1],
    'term_ornaments' => q[1],
    'test_report' => q[0],
    'trust_test_report_history' => q[0],
    'unzip' => q[],
    'urllist' => [q[http://mirror.pop-sc.rnp.br/CPAN/], q[http://cpan.dcc.uchile.cl/], q[http://cpan.mmgdesigns.com.ar/]],
    'use_sqlite' => q[0],
    'version_timeout' => q[15],
    'wget' => q[/usr/bin/wget],
    'yaml_load_code' => q[0],
    'yaml_module' => q[YAML],
    '};'
    '1;'
    '__END__'
    " > /root/.cpan/CPAN/MyConfig.pm

    export PERL_MM_USE_DEFAULT=1

    echo "install Bundle::CPAN CGI LWP Net::Ping Rcs Mail::Sender MIME::Base64 Net::Telnet Net::Telnet::Cisco Text::ParseWords Template" > /tmp/perlmodules.tmp

    perl -MCPAN -e shell < /tmp/perlmodules.tmp

    wget http://mtfln.multitask.com.br/install/inst_mtadm.new
    wget http://mtfln.multitask.com.br/install/inst_mtmon_client.new

    sh /root/fb/inst_mtadm.new
    sh /root/fb/inst_mtmon_client.new

    touch /var/spool/cron/root && chmod 600 /var/spool/cron/root

    echo "59 * * * * /usr/local/Multitask/mtadm/bin/MonitoraAmbiente.sh" >> /var/spool/cron/root
    echo "30 6,12,18 * * * /usr/local/Multitask/mtadm/bin/mtwdog_files.pl --sincronizar" >> /var/spool/cron/root
    echo "*/15 * * * * /usr/local/Multitask/mtadm/bin/ColetaMemoriaReal.sh" >> /var/spool/cron/root
    echo "@reboot /usr/local/Multitask/mtmon/bin/mtmon_cron.pl" >> /var/spool/cron/root
    echo "0,15,30,45 * * * * /usr/local/Multitask/mtmon/bin/mtmon_cron.pl" >> /var/spool/cron/root

    systemctl restart crond

    echo "[Global]" > /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Origem=fbcon" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Server=$NOME_CLIENTE" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  ServerMTMON=177.19.228.142, 189.72.110.218, 200.175.53.1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Proxy=" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  TransKEY=frgw5z_x9VtE" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Atributos=site,producao" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Debug=0" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  NaoMostrarLicenca=1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Producao=1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  sarMeses=24" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  ServerLicenca=170.84.17.136, 177.19.228.142, 189.72.110.218, 200.175.53.1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  ServerSMS=189.72.110.218, 200.175.53.1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  SiteBKP=170.84.17.136" >> /usr/local/Multitask/mtadm/etc/mtadm.conf

    /usr/local/bin/cpzip

    /usr/local/Multitask/mtmon/bin/mtmon_cron.pl -debug

    /usr/local/Multitask/mtadm/bin/IncluiArquivos.pl -a mtwdog_files.pl --lista|grep -i auth

    if [ '$(cat /etc/zabbix/zabbix_agentd.conf |grep -v ^# |grep "Server=")' = "Server=127.0.0.1" ]; then
    
        sed 's/Server=127.0.0.1/Server=127.0.0.1,35.198.3.197/g' -i /etc/zabbix/zabbix_agentd.conf

    else

        server=$(cat /etc/zabbix/zabbix_agentd.conf |grep -v ^'#' |grep "Server=")

        sed "s/$server/$server,35.198.3.197/g" -i /etc/zabbix/zabbix_agentd.conf

    fi

    systemctl restart zabbix-agent

    continuar

}

function install_dnsrev {

    clear

    echo ""
    read -p " Insira o nome do cliente para criacao da ACL Policy: " NOME_CLIENTE
    echo ""

    echo ""
    echo " Insira a range do cliente para criacao da ACL Policy:"
    echo " ex: 201.182.124.0/22"
    echo ""
    read -p "   Range: " IPRANGE

    echo ""
    echo " Insira o endereço DNS do cliente que respondera pelas ranges:"
    echo " ex: technik.net.br"
    echo ""
    read -p "   FQDN: " FQDN

    echo ""
    read -p " Insira o e-mail de contato do cliente: " emailcontato
    echo ""

    emailcontato=$(echo $emailcontato |sed 's/@/./g')

    yum install bind -y

    mkdir /var/log/named/

    mkdir /var/named/

    chcon system_u:object_r:var_log_t:s0 /var/log/named

    setenforce 1

    chown named:named /var/log/named
    chown named:named /var/named/

    cp /root/fb/named.conf /etc/named.conf

    sed "s/nome_cliente/$NOME_CLIENTE/g" -i /etc/named.conf

    sed "s;range_cliente;$IPRANGE;g" -i /etc/named.conf

    echo ""
    echo " Insira as ranges do cliente que serao configuradas no reverso, separadas por virgula e sem o ultimo octeto."
    echo ""
    echo "ex: 45.111.222,45.111.223,45.111.224,45.111.225"
    echo ""
    read -p "   IPs: " IPSCLIENTE

    ttl='$ttl'
    GENERATE='$GENERATE'

    for ip in $(echo $IPSCLIENTE |sed 's/,/\n/g'); do

        touch /var/named/rev.$oct1.$oct2.$oct3

        oct1=$(echo $ip |cut -f1 -d'.')
        oct2=$(echo $ip |cut -f2 -d'.')
        oct3=$(echo $ip |cut -f3 -d'.')

        REVFILE="/var/named/rev.$oct1.$oct2.$oct3"

        echo "
        $ttl 38400
        $oct3.$oct2.$oct1.in-addr.arpa.        IN      SOA     ns3.$FQDN. $emailcontato. (
                                1579875781
                                10800
                                3600
                                604800
                                38400 ) 
        $oct3.$oct2.$oct1.in-addr.arpa.        IN      NS      ns3.$FQDN.
        $oct3.$oct2.$oct1.in-addr.arpa.        IN      NS      ns4.$FQDN.
        $GENERATE 0-255 $ PTR $oct1.$oct2.$oct3.$.$FQDN.
        " |sed 's/^        //g' >> $REVFILE

        echo zone "$oct3.$oct2.$oct1.in-addr.arpa" { >> /etc/named.conf
        echo "         type master;" >> /etc/named.conf
        echo "         file "/var/named/rev.$oct1.$oct2.$oct3";" >> /etc/named.conf
        echo '};' >> /etc/named.conf

        #echo zone "$oct3.$oct2.$oct1.in-addr.arpa" { >> /etc/named.conf
        #echo "         type master;" >> /etc/named.conf
        #echo           file "/var/named/rev.$oct1.$oct2.$oct3"; >> /etc/named.conf
        #echo '};' >> /etc/named.conf 

    done

    systemctl enable named && systemctl start named
 
}

function install_zbx_telegram {

    clear

    echo ""
    read -p " Insira o ID do grupo do telegram: " GROUP_ID
    echo ""

    echo ""
    read -p " Insira o TOKEN do bot: " TOKEN_BOT
    echo ""

    cp /root/fb/zabbix-telegram /usr/lib/zabbix/alertscripts/zabbix-telegram.sh

    chmod /usr/lib/zabbix/alertscripts/zabbix-telegram.sh

    zbx_dbuser=$(cat /etc/zabbix/zabbix_server.conf |grep -i ^dbuser |cut -f2 -d"=")

    zbx_dbpass=$(cat /etc/zabbix/zabbix_server.conf |grep -i ^dbpass |cut -f2 -d"=")

    IP_SERVER=$(ifconfig |grep -w inet |grep -v "127.0.0.1" |awk {'print $2'})

    mysql -u zabbix --pass=$zbx_dbpass $zbx_dbuser -e "INSERT INTO `media_type` VALUES (5,1,'Telegram','','','','zabbix-telegram.sh','','','',0,25,0,0,0,0,'{ALERT.SENDTO}\n{ALERT.SUBJECT}\n{ALERT.MESSAGE}\n',1,3,'10s',1);"
    
    mysql -u zabbix --pass=$zbx_dbpass $zbx_dbuser -e "INSERT INTO `users` VALUES (6,'telegram','telegram','','3e49316528a99784f57ea14741ecc046','',0,'0','en_GB','30s',3,'default',0,'',0,50);"

    mysql -u zabbix --pass=$zbx_dbpass $zbx_dbuser -e "INSERT INTO `media` VALUES (3,6,5,'-"$GROUP_ID"',0,60,'1-7,00:00-24:00');"

    zbx_telegram_pass="UvRAUG7yUq4t"

    sed "s/IP_SERVER/$IP_SERVER/g" -i /usr/lib/zabbix/alertscripts/zabbix-telegram.sh

    sed "s/TOKEN_BOT/$TOKEN_BOT/g" -i /usr/lib/zabbix/alertscripts/zabbix-telegram.sh
}

function menu {

    echo "   ####################################"
    echo "   ##                                ##"
    echo "   ##      FB Package Installer      ##"
    echo "   ##                                ##"
    echo "   ####################################"
    echo ""
    echo ""
    echo "       1) Boas Praticas"
    echo "       2) Cores do terminal"
    echo "       3) FB - Message of the Day"
    echo "       4) Instalar Servidor Zabbix"
    echo "       5) Instalar Servidor FTP/BKP"
    echo "       6) Instalar Servidor Rsyslog"
    echo "       7) Instalar WebMin"
    echo "       8) Instalar Monitoramento MTMon"
    echo "       9) Instalar Servidor DNS-REVERSO"
    echo "       0) sair"
    echo ""
    echo ""
    echo ""

    read -p " -> Digite o numero da opcao que sera executada: " OP_MENU

    case $OP_MENU in

        1) best_practices ;;
        2) bashrc ;;
        3) neofetch ;;
        4) install_zabbix ;;
        5) install_ftpbkp ;;
        6) install_rsyslog ;;
        7) install_webmin ;;
        8) install_mtmon ;;
        9) install_dnsrev ;;
        0) exit 0 ;;
        *) menu ;;

    esac

}

clear

menu