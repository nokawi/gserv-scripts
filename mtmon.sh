while getopts "a:b:c:" opt
    do
    case "$opt" in
         a ) NOME_CLIENTE="$OPTARG" ;;
     esac
done

function install_mtmon {

    echo "$NOME_CLIENTE"

    while [ -z $NOME_CLIENTE ];do

        echo ""
        echo "O nome do cliente nao pode ficar em branco..."
        echo ""

        read -p "Digite o nome do cliente/servidor: " NOME_CLIENTE

    done

    yum -y install perl-CPAN perl-LWP-Protocol-https.noarch perl-Crypt-SSLeay.x86_64 perl-CGI.noarch perl-JSON.noarch perl-JSON-PP.noarch
    yum -y install rcs sharutils sysstat wget
    yum -y install gcc m4 awk flex bison expect

    unset PERL5LIB
    unset PERL_LOCAL_LIB_ROOT
    #perl -MCPAN -e shell

    mkdir -p /root/.cpan/CPAN/ && touch /root/.cpan/CPAN/MyConfig.pm

    CPAN='$CPAN'

    echo "
    '$CPAN'::Config = {'
    'applypatch' => q[],
    'auto_commit' => q[0],
    'build_cache' => q[100],
    'build_dir' => q[/root/.cpan/build],
    'build_dir_reuse' => q[0],
    'build_requires_install_policy' => q[yes],
    'bzip2' => q[],
    'cache_metadata' => q[1],
    'check_sigs' => q[0],
    'colorize_output' => q[0],
    'commandnumber_in_prompt' => q[1],
    'connect_to_internet_ok' => q[1],
    'cpan_home' => q[/root/.cpan],
    'ftp_passive' => q[1],
    'ftp_proxy' => q[],
    'getcwd' => q[cwd],
    'gpg' => q[/usr/bin/gpg],
    'gzip' => q[/usr/bin/gzip],
    'halt_on_failure' => q[0],
    'histfile' => q[/root/.cpan/histfile],
    'histsize' => q[100],
    'http_proxy' => q[],
    'inactivity_timeout' => q[0],
    'index_expire' => q[1],
    'inhibit_startup_message' => q[0],
    'keep_source_where' => q[/root/.cpan/sources],
    'load_module_verbosity' => q[none],
    'make' => q[/usr/bin/make],
    'make_arg' => q[],
    'make_install_arg' => q[],
    'make_install_make_command' => q[/usr/bin/make],
    'makepl_arg' => q[],
    'mbuild_arg' => q[],
    'mbuild_install_arg' => q[],
    'mbuild_install_build_command' => q[./Build],
    'mbuildpl_arg' => q[],
    'no_proxy' => q[],
    'pager' => q[/usr/bin/less],
    'patch' => q[],
    'perl5lib_verbosity' => q[none],
    'prefer_external_tar' => q[1],
    'prefer_installer' => q[MB],
    'prefs_dir' => q[/root/.cpan/prefs],
    'prerequisites_policy' => q[follow],
    'scan_cache' => q[atstart],
    'shell' => q[/bin/bash],
    'show_unparsable_versions' => q[0],
    'show_upload_date' => q[0],
    'show_zero_versions' => q[0],
    'tar' => q[/usr/bin/tar],
    'tar_verbosity' => q[none],
    'term_is_latin' => q[1],
    'term_ornaments' => q[1],
    'test_report' => q[0],
    'trust_test_report_history' => q[0],
    'unzip' => q[],
    'urllist' => [q[http://mirror.pop-sc.rnp.br/CPAN/], q[http://cpan.dcc.uchile.cl/], q[http://cpan.mmgdesigns.com.ar/]],
    'use_sqlite' => q[0],
    'version_timeout' => q[15],
    'wget' => q[/usr/bin/wget],
    'yaml_load_code' => q[0],
    'yaml_module' => q[YAML],
    '};'
    '1;'
    '__END__'
    " > /root/.cpan/CPAN/MyConfig.pm

    export PERL_MM_USE_DEFAULT=1

    echo "install Bundle::CPAN CGI LWP Net::Ping Rcs Mail::Sender MIME::Base64 Net::Telnet Net::Telnet::Cisco Text::ParseWords Template" > /tmp/perlmodules.tmp

    perl -MCPAN -e shell < /tmp/perlmodules.tmp

    wget http://mtfln.multitask.com.br/install/inst_mtadm.new
    wget http://mtfln.multitask.com.br/install/inst_mtmon_client.new

    #wget https://mttmb.multitask.com.br/install/mtmon/inst_mtadm.5.4.new.gz
    #wget https://mttmb.multitask.com.br/install/mtmon/inst_mtmon_client.5.5.new.gz
    #wget https://mttmb.multitask.com.br/install/mtmon/inst_mtsend.1.55.new.gz

    sh inst_mtadm.new
    sh inst_mtmon_client.new

    chmod 777 inst_mtadm.new inst_mtmon_client.neww

    touch /var/spool/cron/root && chmod 600 /var/spool/cron/root

    echo "59 * * * * /usr/local/Multitask/mtadm/bin/MonitoraAmbiente.sh" >> /var/spool/cron/root
    echo "30 6,12,18 * * * /usr/local/Multitask/mtadm/bin/mtwdog_files.pl --sincronizar" >> /var/spool/cron/root
    echo "*/15 * * * * /usr/local/Multitask/mtadm/bin/ColetaMemoriaReal.sh" >> /var/spool/cron/root
    echo "@reboot /usr/local/Multitask/mtmon/bin/mtmon_cron.pl" >> /var/spool/cron/root
    echo "0,15,30,45 * * * * /usr/local/Multitask/mtmon/bin/mtmon_cron.pl" >> /var/spool/cron/root

    systemctl restart crond

    echo "[Global]" > /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Origem=fbcon" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Server=$NOME_CLIENTE" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  ServerMTMON=177.19.228.142, 189.72.110.218, 200.175.53.1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Proxy=" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  TransKEY=frgw5z_x9VtE" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Atributos=site,producao" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Debug=0" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  NaoMostrarLicenca=1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  Producao=1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  sarMeses=24" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  ServerLicenca=170.84.17.136, 177.19.228.142, 189.72.110.218, 200.175.53.1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  ServerSMS=189.72.110.218, 200.175.53.1" >> /usr/local/Multitask/mtadm/etc/mtadm.conf
    echo "  SiteBKP=170.84.17.136" >> /usr/local/Multitask/mtadm/etc/mtadm.conf

    /usr/local/bin/cpzip

    /usr/local/Multitask/mtmon/bin/mtmon_cron.pl -debug

   # /usr/local/Multitask/mtadm/bin/IncluiArquivos.pl -a mtwdog_files.pl --lista|grep -i auth

   /usr/local/Multitask/mtadm/bin/IncluiArquivos.pl -a mtwdog_files.pl --lista

    if [ '$(cat /etc/zabbix/zabbix_agentd.conf |grep -v ^# |grep "Server=")' = "Server=127.0.0.1" ]; then
    
        sed 's/Server=127.0.0.1/Server=127.0.0.1,35.198.3.197/g' -i /etc/zabbix/zabbix_agentd.conf

    else

        server=$(cat /etc/zabbix/zabbix_agentd.conf |grep -v ^'#' |grep "Server=")

        sed "s/$server/$server,35.198.3.197/g" -i /etc/zabbix/zabbix_agentd.conf

    fi

    systemctl restart zabbix-agent

    system root detected

}

install_mtmon
