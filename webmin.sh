function install_webmin {
    echo "[Webmin]" > /etc/yum.repos.d/webmin.repo
    echo "name=Webmin Distribution Neutral" >> /etc/yum.repos.d/webmin.repo 
    echo "baseurl=http://download.webmin.com/download/yum" >> /etc/yum.repos.d/webmin.repo
    echo "enabled=1" >> /etc/yum.repos.d/webmin.repo
    echo "gpgcheck=1" >> /etc/yum.repos.d/webmin.repo
    echo "gpgkey=http://www.webmin.com/jcameron-key.asc" >> /etc/yum.repos.d/webmin.repo
    
    yum -y install webmin

    sed 's/port=10000/port=8079/g' -i /etc/webmin/miniserv.conf
    sed 's/listen=10000/listen=8079/g' -i /etc/webmin/miniserv.conf
    sed 's/ipv6=1/ipv6=0/g' -i /etc/webmin/miniserv.conf
    
    firewall-cmd --permanent --add-port=10000/tcp
    firewall-cmd --reload-all

}

install_webmin
