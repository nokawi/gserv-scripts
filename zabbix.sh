while getopts "a:b:c:" opt
    do
    case "$opt" in
         a ) NOME_ZABBIX="$OPTARG" ;;
     esac
done

function install_zabbix {

    clear

    function nao_usada {

        echo ""
        echo ""
        echo " 1 - prod     | ambientes de fora da empresa que utilizarao o ip 186.251.23.230 na porta 9901"
        echo ""
        echo " 2 - dev      | ambientes internos que utilizarao o ip 192.168.2.248 na porta 8080"
        echo ""
        echo " 3 - local    | ambiente local de testes em localhost na porta 8080"
        echo ""
        echo ""
        read -p " Escolha uma das opcoes de ambiente para instalacao: " ENV

        case ENV in
            1)
                fb_server="186.251.23.230:9901"
            ;;

            2)
                fb_server="192.168.2.248:8080"
            ;;

            3)
                fb_server="localhost:8080"
            ;;
        esac

    }

    echo ""
    echo ""
    echo "$NOME_ZABBIX"
    echo ""
    echo ""

    counter="10"

    while [ "$counter" != "-1" ]; do

            echo -en "\r Iniciando instalacao do ambiente Zabbix em $counter segundos..."

            sleep 1

            let "counter=$counter-1"

    done

    echo ""
    echo ""

    echo "Iniciando configuracao do MariaDB..."

    yum install mariadb-server mariadb httpd php -y

    sed 's/\[mysqld]/\[mysqld]\nmax_connections = 2000/g' -i /etc/my.cnf.d/server.cnf

    systemctl start mariadb.service

    db_root_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    db_zabbix_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    echo "
    UPDATE mysql.user SET Password=PASSWORD('${db_root_password}') WHERE User='root';
    DELETE FROM mysql.user WHERE User='';
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
    DROP DATABASE IF EXISTS test;
    DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
    FLUSH PRIVILEGES;
    CREATE DATABASE zabbix CHARACTER SET utf8 COLLATE utf8_bin; 
    GRANT ALL PRIVILEGES ON zabbix.* TO 'zabbix'@'%' IDENTIFIED BY '${db_zabbix_password}';
    SET GLOBAL max_connections = 500;
    " > /tmp/mariadb.config.tmp && mysql -u root < /tmp/mariadb.config.tmp

    echo "Iniciando instalacao do Zabbix"

    # VERSAO 3.0
    # rpm -ivh https://repo.zabbix.com/zabbix/3.0/rhel/7/x86_64/zabbix-release-3.0-1.el7.noarch.rpm

    # Versao 4.0.2
    rpm -Uvh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-4.0-2.el7.noarch.rpm

    yum install zabbix-server-mysql zabbix-web-mysql zabbix-agent zabbix-get -y

    sed 's;# php_value date.timezone Europe/Riga;php_value date.timezone America/Sao_Paulo;g' -i /etc/httpd/conf.d/zabbix.conf

    sed "s;# DBPassword=;DBPassword=$db_zabbix_password;g" -i /etc/zabbix/zabbix_server.conf
    sed 's/# DBHost=localhost/DBHost=localhost/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# StartPollers=5/StartPollers=16/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# StartPollersUnreachable=1/StartPollersUnreachable=32/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# StartPingers=1/StartPingers=32/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# HousekeepingFrequency=1/HousekeepingFrequency=3/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# MaxHousekeeperDelete=5000/MaxHousekeeperDelete=0/g' -i /etc/zabbix/zabbix_server.conf
    sed 's/# CacheSize=8M/CacheSize=16M/g' -i /etc/zabbix/zabbix_server.conf

    zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -u zabbix --pass=$db_zabbix_password zabbix

    echo ""
    echo ""

    cp zabbix.conf.php /etc/zabbix/web/zabbix.conf.php
    sed "s;db_zabbix_password;$db_zabbix_password;g" -i /etc/zabbix/web/zabbix.conf.php
    sed "s;NOME_ZABBIX;$NOME_ZABBIX;g" -i /etc/zabbix/web/zabbix.conf.php

  
    chown apache:apache /etc/zabbix/web/zabbix.conf.php

    systemctl enable httpd zabbix-server zabbix-agent mariadb

    echo ""
    echo "Instalacao concluida..."
    echo ""
    echo "Senha do MariaDB: $db_root_password"
    echo ""
    echo "Senha do Zabbix para DB: $db_zabbix_password"
    echo ""
    echo "Acesso WEB:"
    echo "  user = Admin"
    echo "  pass = zabbix (trocar no primeiro acesso para m7...)"
    echo ""
    echo ""
    echo " REINICIE O SERVIDOR PARA PODER ACESSAR O ZABBIX VIA WEB"


}

install_zabbix
