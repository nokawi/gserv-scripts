function install_rsyslog {

    curl http://rpms.adiscon.com/v8-stable/rsyslog.repo > /etc/yum.repos.d/rsyslog.repo && yum install rsyslog* --skip-broken -y

    yum install mariadb-server mariadb httpd wget php php-mysql php-gd -y

    sed 's/\[mysqld]/\[mysqld]\nmax_connections = 2000/g' -i /etc/my.cnf.d/server.cnf

    systemctl start mariadb.service

     # db_root_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

     # db_syslog_password=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)


    db_root_password="5R9BRupowR&"

    db_syslog_password="5R9BRupowR&"

    echo "
    UPDATE mysql.user SET Password=PASSWORD('${db_root_password}') WHERE User='root';
    DELETE FROM mysql.user WHERE User='';
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
    DROP DATABASE IF EXISTS test;
    DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
    FLUSH PRIVILEGES;
    SET GLOBAL max_connections = 500;
    " > /tmp/mariadb.config.tmp && mysql -u root < /tmp/mariadb.config.tmp

    echo "
        CREATE DATABASE Syslog;
        USE Syslog;
        CREATE TABLE SystemEvents      
        (
            ID int unsigned not null auto_increment primary key,
            CustomerID bigint,
            ReceivedAt datetime NULL,
            DeviceReportedTime datetime NULL,
            Facility smallint NULL,
            Priority smallint NULL,
            FromHost varchar(60) NULL,
            Message text,
            NTSeverity int NULL,
            Importance int NULL,
            EventSource varchar(60),
            EventUser varchar(60) NULL,
            EventCategory int NULL,
            EventID int NULL,
            EventBinaryData text NULL,
            MaxAvailable int NULL,
            CurrUsage int NULL,
            MinUsage int NULL,
            MaxUsage int NULL,
            InfoUnitID int NULL ,
            SysLogTag varchar(60),
            EventLogType varchar(60),
            GenericFileName VarChar(60),
            SystemID int NULL
        );

        CREATE TABLE SystemEventsProperties
        (
            ID int unsigned not null auto_increment primary key,
            SystemEventID int NULL ,
            ParamName varchar(255) NULL ,
            ParamValue text NULL
        );

    " > /tmp/mariadb.config.tmp && mysql -u root -p$db_root_password < /tmp/mariadb.config.tmp

    mysql -u root -p $db_root_password -e "GRANT ALL ON Syslog.* TO 'rsyslogdbadmin'@'localhost' IDENTIFIED BY '$db_syslog_password';"

    echo 'module(load="imuxsock")' > /etc/rsyslog.conf
    echo 'module(load="imklog")' >> /etc/rsyslog.conf
    echo 'module(load="imudp")' >> /etc/rsyslog.conf
    echo 'input(type="imudp" port="514")' >> /etc/rsyslog.conf
    echo 'module(load="imtcp")' >> /etc/rsyslog.conf
    echo 'input(type="imtcp" port="514")' >> /etc/rsyslog.conf
    echo 'module(load="ommysql")' >> /etc/rsyslog.conf
    echo '$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat' >> /etc/rsyslog.conf
    echo '$IncludeConfig /etc/rsyslog.d/*.conf' >> /etc/rsyslog.conf
    echo '*.info;mail.none;authpriv.none;cron.none /var/log/messages' >> /etc/rsyslog.conf
    echo 'authpriv.* /var/log/secure' >> /etc/rsyslog.conf
    echo 'mail.* /var/log/maillog' >> /etc/rsyslog.conf
    echo 'cron.* /var/log/cron' >> /etc/rsyslog.conf
    echo '*.emerg :omusrmsg:*' >> /etc/rsyslog.conf
    echo 'uucp,news.crit /var/log/spooler' >> /etc/rsyslog.conf
    echo 'local7.* /var/log/boot.log' >> /etc/rsyslog.conf
    echo "*.* :ommysql:127.0.0.1,Syslog,rsyslogdbadmin,$db_syslog_password" >> /etc/rsyslog.conf

    wget -q -O- $(curl https://loganalyzer.adiscon.com/downloads/ |grep '.tar.gz' |cut -f2 -d'"') | tar -xvzf -

    mkdir -p /var/www/html/log

    arquivo=$(ls |grep loganalyzer |grep -v .sql)

    cp -r $arquivo/src/* /var/www/html/log/
    cp -r $arquivo/contrib/* /var/www/html/log/

    echo '
    <Directory "/var/www/html/log">
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
    </Directory>
    ' > /etc/httpd/conf.d/log_server.conf

    systemctl restart rsyslog
    systemctl restart httpd

    cp config.php /var/www/html/log/;
    chmod 666 /var/www/html/log/config.php;

    sed "s/trocarsenha/$db_syslog_password/g" -i /var/www/html/log/config.php

    mysql -u root -p $db_root_password Syslog < /root/loganalyzer.sql

    mysql -u root -p $db_root_password Syslog -e "update logcon_sources set DBPassword='"$db_syslog_password"' where DBUser='rsyslogdbadmin';"

    loganalyzer_admin_pass=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w16 | head -n1)

    mysql -u root -p $db_root_password Syslog -e "update logcon_users set password=MD5('"$loganalyzer_admin_pass"') where username='admin';"

    echo "mysql --user=rsyslogdbadmin --password="$db_syslog_password" --database=Syslog --execute='DELETE FROM Syslog.SystemEvents WHERE ReceivedAt < DATE_SUB(NOW(), INTERVAL 365 DAY);'" > /root/limpalog.sh

    chmod +x /root/limpalog.sh

    echo "00 00  1 * * root sh /root/limpalog.sh" >> /etc/crontab && systemctl restart crond

    echo " root= $db_root_password"
    echo " rsyslogdbadmin = $db_syslog_password"
    echo " Admin (web) = $loganalyzer_admin_pass"


}

install_rsyslog
