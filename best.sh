#!/bin/bash


function best_practices {

    if [ $(systemctl status firewalld |grep Active: | awk -d":" {'print $2'}) != "inactive" ]; then

        systemctl disable firewalld ; systemctl stop firewalld

    fi

    if [[ "$(cat /etc/selinux/config |grep '^SELINUX=' |cut -d"=" -f2)" =~ [Ee]nforcing ]]; then

        sed 's/[eE]nforcing/disabled/g' -i /etc/selinux/config

    fi


    pacotes="
    htop
    net-tools
    net-snmp
    net-snmp-devel
    net-snmp-utils
    wget
    iputils
    traceroute
    vim
    nano
    git
    mlocate
    epel-release
    bind-utils
    tcpdump
    iotop
    nload
    multitail
    telnet
    nmap
    "

    for package in $pacotes; do

        if [ $(rpm -qa --qf "%{NAME}\n" |egrep -w "$package" |head -1 |wc -l) = "0" ]; then

            pk_install="$pk_install@$package"
        fi

    done

    if [ ! -z $pk_install ]; then

        pk_install=$(echo $pk_install |sed 's/@/ /g')

        yum update -y && yum install $pk_install -y

    fi

    echo "Instalação bem sucedida, pacotes instalados com sucesso!"

}


best_practices

