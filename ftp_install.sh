#!/bin/bash

while getopts "a:b:c:" opt
    do
    case "$opt" in
         a ) NOME_CLIENTE="$OPTARG" ;;
     esac
done

function install_ftpbkp {

    echo "$NOME_CLIENTE"

    yum install httpd php wget openssl-devel.x86_64 nmap mysql sshpass ncftp ftp mariadb-server mariadb net-tools -y

    systemctl disable firewalld

    systemctl start httpd ; systemctl enable httpd

    yum install proftpd -y

    echo "Port 4720" >> /etc/proftpd.conf

    systemctl enable proftpd && systemctl start proftpd

    if [ $(cat /etc/shells |grep "/sbin/nologin" |wc -l) = "0" ]; then

        echo "/sbin/nologin" >> /etc/shells

    fi

    mkdir -p /var/ftp-$NOME_CLIENTE/backup
    useradd $NOME_CLIENTE -M -d /var/ftp-$NOME_CLIENTE
    chown -R $NOME_CLIENTE:$NOME_CLIENTE /var/ftp-$NOME_CLIENTE
    chmod -R 0777 /var/ftp-$NOME_CLIENTE/

    ftp_pass=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    echo $NOME_CLIENTE:$ftp_pass | chpasswd

    if [ $(cat /var/spool/cron/root |grep '-mtime +90' |wc -l) = "0" ] && [ $(cat /var/spool/cron/root |grep '-mtime +90' |wc -l) -ge "1" ] ; then

        sed -i '/$NOME_CLIENTE/d' /var/spool/cron/root

        echo "0 6 * * * find /home/$NOME_CLIENTE/backup/*.rsc -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root
        echo "0 6 * * * find /home/$NOME_CLIENTE/backup/*.backup -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root
        echo "0 6 * * * find /home/$NOME_CLIENTE/ANM/*.zip -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root
        echo "0 6 * * * find /home/$NOME_CLIENTE/OLT/*.cfg -mtime +90 -exec rm -f {} \;" >> /var/spool/cron/root

        chmod 600 /var/spool/cron/root

        systemctl restart crond

    fi

    mkdir /var/www/html/ftp

    htpasswd -c -b /etc/httpd/.htpasswd fb "#fb#$NOME_CLIENTE"

    htpasswd -b /etc/httpd/.htpasswd $NOME_CLIENTE "$ftp_pass"

    chown apache:apache /etc/httpd/.htpasswd

    mkdir /var/www/html/ftp

    cp ftp_server.txt /var/www/html/ftp/index.php

    #wget http://186.251.23.230:9901/ftp_server.txt -O /var/www/html/ftp/index.php

    sed "s;/tmp/backup/;/var/ftp-$NOME_CLIENTE/backup/;g" -i /var/www/html/ftp/index.php

    echo '
    <Directory "/var/www/html/ftp">
        Options Indexes FollowSymLinks
        AuthType Basic
        AuthName "FTP Backup Server - FB Consultoria"
        AuthUserFile /etc/httpd/.htpasswd
        Require valid-user
    </Directory>
    ' > /etc/httpd/conf.d/ftp_server.conf

    echo "date.timezone = America/Sao_Paulo" >> /etc/php.ini

    systemctl restart httpd

    mkdir /root/auto_backup/
    cp backup-equipamentos-cliente.sh /root/auto_backup/
    cp cliente.cfg /root/auto_backup/cliente_fb_backup.cfg

    echo "00 00 * * * root /root/auto_backup/backup-equipamentos-cliente.sh" >> /etc/crontab 

    systemctl restart crond

    IPs=$(ifconfig |grep 'inet ' |grep -v "127.0.0.1" |awk {'print $2'})

    ftpbackup=$(tr -cd '[:lower:][:digit:]' < /dev/urandom | fold -w12 | head -n1)

    systemctl enable httpd mariadb rsyslogd


    echo ""
    echo " Accessos:"
    for cada in $IPs; do
        echo "      http://$cada/ftp"
    done
    echo ""
    echo " FB user: fb"
    echo " FB pass: #fb#$NOME_CLIENTE"
    echo ""
    echo " Cliente user: $NOME_CLIENTE"
    echo " Cliente pass: $ftp_pass"
    echo ""
    echo ""
    echo " Senha de backup que devera ser configurada nos equipamentos para backup automatico: "
    echo " USER: ftpbackup" 
    echo " PASS: $ftpbackup"
    echo ""


}


install_ftpbkp



