function speedtest_install {

	yum install wget iputils traceroute vin nano mlocate epel-release bind-utils unzip policycoreutils-python httpd php bind-utils logrotate curl php.x86_64 nmap-ncat.x86_64 -y 

	systemctl stop firewalld
	

	mkdir /opt/speedtest
	cd /opt/speedtest
	wget https://install.speedtest.net/ooklaserver/ooklaserver.sh
	chmod a+x ooklaserver.sh
	./ooklaserver.sh install -f

	systemctl enable httpd
	systemctl start httpd

	./ooklaserver.sh start

	mkdir /var/www/html/speedtest
	cd /var/www/html/
	wget http://install.speedtest.net/httplegacy/http_legacy_fallback.zip

	unzip http_legacy_fallback.zip
} 

speedtest_install