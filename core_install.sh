#!/bin/bash

function bashrc {
    yum install nano -y

    echo "
    alias ..='cd ..'
    alias rm='rm -i'
    alias cp='cp -i'
    alias mv='mv -i'

    alias grep='grep --color=auto'
    alias egrep='egrep --color=auto'
    alias fgrep='fgrep --color=auto'

    PS1='\[\033[01;31m\]\u\[\033[02;37m\]@\[\033[02;33m\]\h:\[\033[02;36m\]\w\$\[\033[00m\] '

    # Source global definitions
    if [ -f /etc/bashrc ]; then
            . /etc/bashrc
    fi

    " > /root/.bashrc

    cp /root/conf.nanorc /usr/share/nano/

    find /usr/share/nano/ -iname "*.nanorc" -exec echo include {} \; >> /root/.nanorc

}


bashrc
